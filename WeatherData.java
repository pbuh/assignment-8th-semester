package week1;

import java.util.HashSet;
import java.util.Set;

public class WeatherData implements WDataModel, Observable<WDataModel> {

	private Set<Observer<WDataModel>> observers = new HashSet<Observer<WDataModel>>();

	@Override
	public void addObserver(Observer<WDataModel> t) {
		observers.add(t);
	}

	@Override
	public void delObserver(Observer<WDataModel> t) {
		observers.remove(t);
	}

	@Override
	public void notifyObservers() {
		WDataModel c = getChangedValue();

		for (Observer<WDataModel> o : observers)
			o.update(c);
	}

	@Override
	public WDataModel getChangedValue() {
		return this;
	}

	private double humidity;
	private double tempreture;
	private double pressure;

	@Override
	public double getHumidity() {
		return humidity;
	}

	@Override
	public double getTempreture() {
		return tempreture;
	}

	@Override
	public double getPressure() {
		return pressure;
	}

	@Override
	public void setHumidity(double h) {
		this.humidity = h;

		notifyObservers();
	}

	@Override
	public void setPressure(double p) {
		this.pressure = p;

		notifyObservers();
	}

	@Override
	public void setTempreture(double t) {
		this.tempreture = t;

		notifyObservers();
	}

	@Override
	public String toString() {
		return "Tempreture : " + getTempreture() + "\n" + "Pressure : "
				+ getPressure() + "\n" + "Humidity : " + getHumidity();
	}

}
