package week1;

public class StatisticsDisplay implements Display<WDataModel>,
		Observer<WDataModel> {

	@Override
	public void display(WDataModel t) {
		System.out.println("++++++++++++++++++++++++++");
		System.out.println("StatisticsDisplay : ");
		System.out.println(t);
		System.out.println("**************************");
	}

	@Override
	public void update(WDataModel u) {
		display(u);
	}
}
