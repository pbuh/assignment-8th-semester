package week1;

public class TestWeatherData {

	public static void main(String[] args) {
		WeatherData wdata = new WeatherData();

		ForecastDisplay fdis = new ForecastDisplay();
		StatisticsDisplay sdis = new StatisticsDisplay();
		CurrentConditionsDisplay cdis = new CurrentConditionsDisplay();

		wdata.addObserver(fdis);
		wdata.addObserver(sdis);
		wdata.addObserver(cdis);

		wdata.setHumidity(12);
		wdata.setPressure(45);
		wdata.setTempreture(35);
	}
}
