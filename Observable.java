package week1;

public interface Observable<T> {

	void addObserver(Observer<T> t);

	void delObserver(Observer<T> t);

	void notifyObservers();

	T getChangedValue();
}
