package week1;

public interface WDataModel {

	double getHumidity();

	double getTempreture();

	double getPressure();

	void setHumidity(double h);

	void setPressure(double p);

	void setTempreture(double t);

}
