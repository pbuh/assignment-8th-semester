package week1;

public interface Observer<T> {

	public void update(T u);
}
