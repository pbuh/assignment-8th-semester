package week2;

public class Main {
	public static void main(String[] args) {
		DepartmentManager dep = new DepartmentManager();

		SoftwareEngineering sw = new SoftwareEngineering();
		dep.addNewDepartment(sw);

		Database db = new Database();
		dep.addNewDepartment(db);

		Network nt = new Network();
		dep.addNewDepartment(nt);

		Student sha = new Student(" Sayed Shah Agha", "Fazil");
		dep.registerStudent(sw.getIdCode(), sha);

		Student evaz = new Student("Tahira", "Mohammadi");
		dep.registerStudent(db.getIdCode(), evaz);

		Student reza = new Student("Said Reza", "Hossaini");
		dep.registerStudent(nt.getIdCode(), reza);
	}
}
