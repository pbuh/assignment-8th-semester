package week2;

public class Network extends Department {

	@Override
	protected String getName() {
		return "Network";
	}

	@Override
	protected Class[] getClasses() {
		return new Class[]{
				new Class(4, "NTFC4"),
				new Class(4, "MATH4"),
				new Class(5, "NET"),
				new Class(2, "ENGLISH"),
				new Class(2, "ISLMIC"),
		};
	}

	@Override
	public String getIdCode() {
		return "NETWORK";
	}

}
