package week2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public abstract class Department {

	private Class[] classes = getClasses();
	private Map<Student, String> studentEmails = new HashMap<Student, String>();

	public void registerStudent(Student newStudent) {
		fillCreditSheet(newStudent);
		createEduEmail(newStudent);
	}

	private void fillCreditSheet(Student newStudent) {
		System.out.println("\n\nHi : " + newStudent.getFullName());
		System.out.println("\nSelect the credits to enroll : ");

		System.out.println("\nID --> CRDT, NAME");

		int n = 1;
		for (Class cls : classes)
			System.out.printf("%2d --> %4d, %-25s\n", n++, cls.getCredit(),
					cls.getName());

		System.out.println("\nType credits id separated by [,] :) ? ");

		BufferedReader tmp0 = new BufferedReader(new InputStreamReader(
				System.in));
		String selectedCredits = "";

		do {
			try {
				selectedCredits = tmp0.readLine();

				for (String crId : selectedCredits.split(","))
					enrollToClass(classes[Integer.parseInt(crId) - 1],
							newStudent);

				System.out.println("Thanks for filling the Credit sheet.\n");

				break;
			} catch (Exception e) {
				System.out
						.println("OoW oW !!!\n" + "Error :(\n" + e.toString());
				System.out.println();
				System.out.println("Try Again : ");
			}
		} while (!selectedCredits.equals("exit"));
	}

	private void enrollToClass(Class theClass, Student newStudent) {
		theClass.addStudent(newStudent);
	}

	private void createEduEmail(Student newStudent) {
		String email = (newStudent.getName() + "." + newStudent.getLastName()
				+ "@" + getIdCode() + ".edu").replaceAll(" ", "_")
				.toLowerCase();
		studentEmails.put(newStudent, email);

		System.out.println("Your Email : " + email);
	}

	public abstract String getIdCode();

	protected abstract String getName();

	protected abstract Class[] getClasses();
}
