package week2;

import java.util.HashMap;
import java.util.Map;

public class DepartmentManager {

	private Map<String, Department> departments = new HashMap<String, Department>();

	public void addNewDepartment(Department newDepartment) {
		departments.put(newDepartment.getIdCode(), newDepartment);
	}

	public void registerStudent(String departmentCode, Student newStudent) {
		departments.get(departmentCode).registerStudent(newStudent);
	}
}
