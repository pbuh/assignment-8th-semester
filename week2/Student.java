package week2;

public class Student {

	private static int autoId = 1;

	private static int nextId() {
		return autoId++;
	}

	// ----------------------

	private int id;
	private String name;
	private String lastName;

	public Student(String name, String lastName) {
		setId(nextId());
		setName(name);
		setLastName(lastName);
	}

	private void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFullName() {
		return getName() + ", " + getLastName();
	}

}
