package week2;

public class Database extends Department {

	@Override
	protected String getName() {
		return "Database";
	}

	@Override
	protected Class[] getClasses() {
		return new Class[]{
				new Class(4, "DBFC4"),
				new Class(4, "MATH4"),
				new Class(5, "DB"),
				new Class(2, "ENGLISH"),
				new Class(2, "ISLMIC"),
		};
	}

	@Override
	public String getIdCode() {
		return "DATABASE";
	}

}
