package week2;

import java.util.HashSet;
import java.util.Set;

public class Class {

	private int credit;
	private String name;
	private Set<Student> students = new HashSet<Student>();

	public Class(int credit, String name) {
		setCredit(credit);
		setName(name);
	}

	private void setCredit(int credit) {
		this.credit = credit;
	}

	private void setName(String name) {
		this.name = name;
	}

	public int getCredit() {
		return credit;
	}

	public String getName() {
		return name;
	}

	public void addStudent(Student newStudent) {
		students.add(newStudent);
	}

	public Set<Student> getStudents() {
		return students;
	}
}
