package week2;

public class SoftwareEngineering extends Department {

	@Override
	protected String getName() {
		return "Software Engineering";
	}

	@Override
	protected Class[] getClasses() {
		return new Class[]{
				new Class(4, "MPFC4"),
				new Class(4, "MATH4"),
				new Class(5, "SW"),
				new Class(2, "ENGLISH"),
				new Class(2, "ISLMIC"),
		};
	}

	@Override
	public String getIdCode() {
		return "SOFT";
	}

}
