package week3.fourth;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FourthQ {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		// change the file name to run test
		String testFileName = "test.txt";

		List<File> files = new ArrayList<File>();
		for (int i = 0; i < 100; i++)
			files.add(new File(testFileName));

		ExecutorService ex = Executors.newFixedThreadPool(files.size());

		long parallelStart = System.currentTimeMillis();
		for (File rand : files)
			ex.execute(new LineCounter(rand));

		ex.shutdown();
		ex.awaitTermination(10, TimeUnit.SECONDS);
		long parallelEnd = System.currentTimeMillis();

		System.out.println("Parallel Done " + files.size() + " In "
				+ (parallelEnd - parallelStart) + " ms.");

		// +++++++++++++++++++++++++++++++++++
		long sequentailStart = System.currentTimeMillis();

		for (File file : files)
			LineCounter.read(file);

		long sequentailEnd = System.currentTimeMillis();
		System.out.println("Sequential Done " + files.size() + " In "
				+ (sequentailEnd - sequentailStart) + " ms.");
	}

}
