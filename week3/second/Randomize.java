package week3.second;

import java.util.List;

public class Randomize implements Runnable {

	private List<Integer> collection;

	public Randomize(List<Integer> collection) {
		this.collection = collection;
	}

	@Override
	public void run() {
		collection.add((int) (Math.random() * 9) + 1);
	}

}
