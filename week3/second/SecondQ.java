package week3.second;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SecondQ {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		List<Integer> collection = new ArrayList<Integer>();

		Randomize[] rands = new Randomize[] { new Randomize(collection),
				new Randomize(collection), new Randomize(collection),
				new Randomize(collection), new Randomize(collection) };

		ExecutorService ex = Executors.newFixedThreadPool(5);

		for (Randomize rand : rands)
			ex.execute(rand);

		ex.shutdown();
		ex.awaitTermination(10, TimeUnit.SECONDS);

		int sum = 0;
		for (Integer rand : collection)
			sum += rand;

		System.out.println(sum);
	}

}
