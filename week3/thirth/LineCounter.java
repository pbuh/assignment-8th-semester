package week3.thirth;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class LineCounter implements Runnable {

	private File file;
	private int lineCount;

	public LineCounter(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	public int getLineCount() {
		return lineCount;
	}

	@Override
	public void run() {
		try {
			BufferedReader buf = new BufferedReader(new FileReader(file));
			while (buf.readLine() != null)
				lineCount++;
			buf.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
