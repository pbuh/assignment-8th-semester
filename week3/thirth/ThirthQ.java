package week3.thirth;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThirthQ {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		List<LineCounter> files = new ArrayList<LineCounter>();

		Scanner input = new Scanner(System.in);

		System.out.println("Enter File Paths, type [exit] to exit : ");

		while (input.hasNextLine()) {
			String path = input.nextLine();

			if (path.equalsIgnoreCase("exit"))
				break;

			File file = new File(path);
			if (file.exists()) {
				files.add(new LineCounter(file));
			} else {
				System.out.println(path + " don't exists :(");
			}
		}

		input.close();

		ExecutorService ex = Executors.newFixedThreadPool(files.size());

		for (LineCounter rand : files)
			ex.execute(rand);

		ex.shutdown();
		ex.awaitTermination(10, TimeUnit.SECONDS);

		int sum = 0;
		for (LineCounter rand : files) {
			System.out.println(rand.getFile().getAbsolutePath());
			System.out.println("line count : " + rand.getLineCount());
			sum += rand.getLineCount();
		}

		System.out.println("Total line count : ");
		System.out.println(sum);
	}

}
