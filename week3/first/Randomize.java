package week3.first;

public class Randomize implements Runnable {

	private int number;

	public int getNumber() {
		return number;
	}

	@Override
	public void run() {
		number = (int) (Math.random() * 9) + 1;
	}

}
