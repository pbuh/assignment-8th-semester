package week3.first;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FirstQ {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {

		Randomize[] rands = new Randomize[] { new Randomize(), new Randomize(),
				new Randomize(), new Randomize(), new Randomize() };

		ExecutorService ex = Executors.newFixedThreadPool(5);

		for (Randomize rand : rands)
			ex.execute(rand);

		ex.shutdown();
		ex.awaitTermination(10, TimeUnit.SECONDS);
		
		int sum = 0;
		for (Randomize rand : rands)
			sum += rand.getNumber();

		System.out.println(sum);
	}

}
