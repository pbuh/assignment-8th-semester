package week1;

public interface Display<T> {

	void display(T t);
}
